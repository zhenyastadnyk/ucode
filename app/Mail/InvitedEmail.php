<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitedEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Invited
     */
    public $invited;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invited)
    {
        $this->invited = $invited;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(  $this->invited->sender)
            ->view('mails.invited')
            ->text('mails.invited');
    }
}