<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Allow cross origin requests.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'X-CALENDAR-TOKEN, Content-Type, X-Auth-Token, Origin, Authorization');
    }
}
