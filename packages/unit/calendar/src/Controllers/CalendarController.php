<?php

namespace unit\calendar\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use unit\calendar\Facades\CalendarHelper;
use unit\calendar\Requests\StoreCalendarPostRequest;
use unit\calendar\Requests\UpdateCalendarPostRequest;
use unit\calendar\Resources\CalendarResource;
use unit\calendar\Resources\ColorResource;
use Illuminate\Http\Request;
use unit\calendar\Models\Calendar;
use unit\calendar\Models\Event;
use unit\calendar\Models\Color;
use Illuminate\Http\Response;

class CalendarController extends Controller
{
    /**
     * @param StoreCalendarPostRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCalendarPostRequest $request)
    {
        if (!$request->user()->id) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_permission')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        try {
            $calendar = CalendarHelper::createCalendar($request, $request->user()->id);

            return response()
                ->json(new CalendarResource($calendar));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateCalendarPostRequest $request
     * @param                           $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCalendarPostRequest $request, $id)
    {
        try {
            $calendar = CalendarHelper::updateCalendar($id, $request);

            if (!$calendar) {
                return response()->json([
                    'message' => ['general' => trans('api.calendar.not_permission')],
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return response()
                ->json(new CalendarResource($calendar));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function delete($id, Request $request)
    {
        $user_id = $request->user()->id;
        try {
            $calendar = CalendarHelper::deleteCalendar($id, $user_id);

            if (!$calendar) {
                return response()->json([
                    'message' => ['general' => trans('api.calendar.not_permission')],
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            if ($calendar) {
                return response(['message' => trans('api.calendar.delete')], Response::HTTP_OK);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|CalendarResource
     */
    public function show($id, Request $request)
    {
        $user = $request->user();
        $calendar = Calendar::where('id', $id)
            ->first();
        if ($calendar && ($calendar->owner_id == $user->id || $calendar->share()->find($user->id))) {
            return new CalendarResource($calendar);
        }

        return response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     *
     * @return AnonymousResourceCollection|JsonResponse|Response
     */
    public function show_all(Request $request)
    {
        $user_id = $request->user()->id;
        $arCalendars = Event::WhereHas("invited", function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
            return $query;
        })->pluck('calendar_id');

        $calendars = Calendar::where('owner_id', $user_id)->orWhereHas("share", function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
                return $query;
            });
        if ($arCalendars) {
            $calendars = $calendars->orwhereIn('id', $arCalendars);
        }
        $calendars = $calendars->get();
        if ($calendars) {
            return CalendarResource::collection($calendars);
        }

        return response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|AnonymousResourceCollection|Response
     */
    public function color()
    {
        $color = Color::all();

        if ($color) {
            return ColorResource::collection($color);
        }
        return response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }
}
