<?php

namespace unit\calendar\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Routing\ResponseFactory;
use unit\calendar\Facades\EventHelper;
use unit\calendar\Requests\StoreEventPostRequest;
use unit\calendar\Requests\UpdateEventPostRequest;
use unit\calendar\Resources\EventResource;
use unit\calendar\Resources\CategoryResource;
use unit\calendar\Models\Event;
use unit\calendar\Models\Category;
use unit\calendar\Models\Calendar;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use unit\calendar\Facades\CalendarHelper;

/**
 * Class EventController
 *
 * @package unit\calendar\Controllers
 */
class EventController extends Controller
{
    /**
     * @param StoreEventPostRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreEventPostRequest $request)
    {
        if ($request->get('calendar') === 'undefined')
            $empty_fields[] = 'calendar';
        if ($request->get('category') === 'undefined')
            $empty_fields[] = 'category';
        if (isset($empty_fields))
            CalendarHelper::emptyFieldsException($empty_fields);

        $calendar = Calendar::where('id', $request->get('calendar'))
            ->firstOrFail();

        if ($request->user()->id != $calendar->owner_id && !$calendar->share()->find($request->user()->id)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_permission')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        try {
            $event = EventHelper::createEvent($request);

            return response()
                ->json(new EventResource($event));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateEventPostRequest $request
     * @param                        $id
     *
     * @return JsonResponse
     */
    public function update(UpdateEventPostRequest $request, $id)
    {
        try {
            $event = EventHelper::updateEvent($id, $request);

            if (!$event) {
                return response()->json([
                    'message' => ['general' => trans('api.calendar.not_permission')],
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return response()
                ->json(new EventResource($event));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|Response
     */
    public function delete($id, Request $request)
    {
        $user_id = $request->user()->id;

        try {
            $event = EventHelper::deleteEvent($id, $user_id);

            if (!$event) {
                return response()->json([
                    'message' => ['general' => trans('api.calendar.not_permission')],
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            if ($event) {
                return response(['message' => trans('api.event.delete')], Response::HTTP_OK);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|Response|EventResource
     */
    public function show($id, Request $request)
    {
        $event = Event::where('id', $id)
            ->first();

        $calendar = Calendar::where('id', $event->calendar_id)
            ->firstOrFail();

        if ($request->user()->id != $calendar->owner_id) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_permission')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($event) {
            return new EventResource($event);
        }

        return response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|AnonymousResourceCollection|Response
     */
    public function show_all(Request $request)
    {
        $user_id = $request->user()->id;
        $calendar_ids = $request->get('calendar_ids');
        if (empty($calendar_ids)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_params')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $calendars = Calendar::whereIn('id', $calendar_ids)->where('owner_id', $user_id)
            ->orWhereHas("share", function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
            return $query;
        })->whereIn('id', $calendar_ids)
            ->pluck('id');
        $invitevents = Event::WhereHas("invited", function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
            return $query;
        })
        ->wherein('calendar_id', $calendar_ids)
        ->pluck('id');

        if ($calendars) {
            $calendars = $calendars->toArray();
        }
        if ($invitevents) {
            $invitevents = $invitevents->toArray();
        }

        if (empty($calendars) && empty($invitevents)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_permission')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        return ($events = EventHelper::getEventsByPeriod($calendars, $request->get('period'),
            $request->get('navigation'), $invitevents))
            ? EventResource::collection($events)
            : response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

    /**
     * @return AnonymousResourceCollection|JsonResponse|Response
     */
    public function category()
    {
        $category = Category::all();

        if ($category) {
            return CategoryResource::collection($category);
        }
        return response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     *
     * @return AnonymousResourceCollection|JsonResponse|Response
     */
    public function search(Request $request)
    {
        $user_id = $request->user()->id;
        $search = $request->get('search');
        $calendar_ids = $request->get('calendar_ids');
        if (!$search && empty($calendar_ids)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_params')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $calendars = Calendar::whereIn('id', $calendar_ids)->where('owner_id', $user_id)
            ->orWhereHas("share", function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
                return $query;
            })->whereIn('id', $calendar_ids)
            ->pluck('id');

        $invitevents = Event::WhereHas("invited", function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
            return $query;
        })
            ->wherein('calendar_id', $calendar_ids)
            ->pluck('id');

        if ($calendars) {
            $calendars = $calendars->toArray();
        }
        if ($invitevents) {
            $invitevents = $invitevents->toArray();
        }

        if (empty($calendars) && empty($invitevents)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_params')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if (empty($calendars) && empty($invitevents)) {
            return response()->json([
                'message' => ['general' => trans('api.calendar.not_params')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return ($events = EventHelper::getEventsSearch($calendars, $search, $invitevents))
            ? EventResource::collection($events)
            : response(['message' => trans('api.error.not_found')], Response::HTTP_NOT_FOUND);
    }

}
