<?php

namespace unit\calendar\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use unit\calendar\Models\Event;
use unit\calendar\Models\Calendar;
use App\User;
use App\Mail\InvitedEmail;
use App\Mail\NotificationEvent;
use Illuminate\Support\Facades\Mail;
use unit\calendar\Facades\CalendarHelper;

class EventHelper
{
    const default_events_period = 'Month';
    const default_events_navigation = 'Today';

    protected static $event_update = [
        'name' => 'name',
        'description'=> 'description',
        'date_from' =>'date_from',
        'date_to' =>'date_to',
        'priority' =>'priority',
        'notes' =>'notes',
    ];


    /**
     * @param Request $request
     * @return Event
     * @throws \Exception
     */
    public function createEvent(Request $request): Event
    {
        $event = new Event;

        foreach (self::$event_update as $code=>$event_params) {
            if ($request->has($event_params)) {
                if (($event_param = $request->get($event_params)) && $event_param !== 'undefined') {
                    $event->{$code} = $event_param;
                }
                else {
                    $empty_fields[] = $code;
                }
            }
        }


        if (isset($empty_fields))
            CalendarHelper::emptyFieldsException($empty_fields);

        $event->category_id = $request->get('category');
        $event->calendar_id = $request->get('calendar');
        $event->save();

        return $event;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return null|EventHelper
     * @throws \Exception
     */
    public function updateEvent(int $id, Request $request)
    {
        /** @var EventHelper $event */
        $event = Event::where('id', $id)
            ->firstOrFail();

        $calendar = Calendar::where('id', $event->calendar_id)
            ->firstOrFail();

        if ($request->user()->id != $calendar->owner_id && !$calendar->share()->find($request->user()->id)
            && !$event->invited()->find($request->user()->id)) {
            return null;
        }

        $event = CalendarHelper::updateModelFields($event, self::$event_update, $request);

        $curInvitedEmail = [];
        $newInvitedEmail = [];
        if ($calendar->share()) {
            $curInvitedEmail = $event->invited()->pluck('email')->toArray();
        }
        if ($request->has('invited')) {
            $newInvitedEmail = $request->get('invited');
        }
        $uninvited = array_diff($curInvitedEmail, $newInvitedEmail);
        $invited = array_diff($newInvitedEmail, $curInvitedEmail);

        if ($invited) {
            foreach ($invited as $email) {
                $user = User::where('email', $email)->first();
                if ($user) {
                    $event->invited()
                        ->attach([$user->id]);

                    $objInvited = new \stdClass();
                    $objInvited->sender = $calendar->owner->email;
                    $objInvited->receiver = $email;
                    $objInvited->event = $event->name;
                    $objInvited->url = env('FRONTEND_URL');
                    Mail::to($user->email)->send(new NotificationEvent($objInvited));
                } else {
                    $calendar = Calendar::where("id", $event->calendar_id)->first();
                    $objInvited = new \stdClass();
                    $objInvited->sender = $calendar->owner->email;
                    $objInvited->receiver = $email;
                    $objInvited->url = env('FRONTEND_URL');
                    $objInvited->event = $event->id;

                    Mail::to($email)->send(new InvitedEmail($objInvited));
                }
            }
        }
        if ($uninvited) {
            foreach ($uninvited as $email) {
                $user = User::where('email', $email)->first();
                $event->invited()
                    ->detach([$user->id]);
            }
        }

        $event->save();

        return $event;
    }


    /**
     * @param integer  $id
     * @param integer  $user_id
     * @return boolean
     */
    public function deleteEvent(int $id, int $user_id)
    {
        $event = Event::where('id', $id)
            ->firstOrFail();

        $calendar = Calendar::where('id', $event->calendar_id)
            ->firstOrFail();

        if ($user_id != $calendar->owner_id) {
            return false;
        }

        $rez = $event->delete();
        return $rez;
    }

    private function getPeriodRange($period, $navigation) {
        $startDate = $navigation !== "Today" ? Carbon::createFromTimeString($navigation) : Carbon::now();

        return [
            'from'  => $startDate->{"startOf$period"}()->toDateTimeString(),
            'to'    => $startDate->{"endOf$period"}()->addSeconds(1)->toDateTimeString()
        ];
    }

    /**
     * @param array       $calendar_ids
     * @param string|null $search
     * @param array  $invitevents
     * @return mixed
     */
    public function getEventsSearch(array $calendar_ids, ?string $search, array $invitevents)
    {
        $events = Event::where('name', 'like', '%'.$search.'%');

        if (!empty($calendar_ids) && !empty($invitevents)) {
            $events = $events->where(function ($query) use ($calendar_ids, $invitevents){
                $query->whereIn('calendar_id', $calendar_ids)->orwhereIn('id', $invitevents);
            });
        } elseif (!empty($calendar_ids)) {
            $events = $events->whereIn('calendar_id', $calendar_ids);
        } else {
            $events = $events->whereIn('id', $invitevents);
        }

        $events = $events->limit(8)->get();
        if ($events) {
            foreach ($events as $event) {
                $event->start = Carbon::createFromTimeString($event->date_from)->getTimestamp();
                $event->end = Carbon::createFromTimeString($event->date_to)->getTimestamp();
            }
        }

        return $events;
    }

    /**
     * @param array       $calendar_ids
     * @param string|null $period
     * @param string|null $navigation
     * @param array  $invitevents
     * @return mixed
     */
    public function getEventsByPeriod(array $calendar_ids, ?string $period, ?string $navigation, array $invitevents)
    {
        $period = $period ?? self::default_events_period;
        $navigation = $navigation ?? self::default_events_navigation;
        $period_range = $this->getPeriodRange($period, $navigation);

        $events = Event::where('date_from', '>=', $period_range['from'])
            ->where('date_to', '<=', $period_range['to']);

        if (!empty($calendar_ids) && !empty($calendar_ids)) {
            $events = $events->where(function ($query) use ($calendar_ids, $invitevents){
                $query->whereIn('calendar_id', $calendar_ids)->orwhereIn('id', $invitevents);
            });
        } elseif (!empty($calendar_ids)) {
            $events = $events->whereIn('calendar_id', $calendar_ids);
        } else {
            $events = $events->whereIn('id', $invitevents);
        }

        $events = $events->get();
        if ($events) {
            foreach ($events as $event) {
                $event->start = Carbon::createFromTimeString($event->date_from)->getTimestamp();
                $event->end = Carbon::createFromTimeString($event->date_to)->getTimestamp();
            }
        }

        return $events;
    }

}
