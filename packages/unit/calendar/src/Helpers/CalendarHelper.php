<?php

namespace unit\calendar\Helpers;

use http\Exception\BadQueryStringException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PHPUnit\Framework\Constraint\ExceptionMessage;
use unit\calendar\Models\Calendar;
use Illuminate\Http\Response;
use App\User;
use App\Mail\NotificationCalendar;
use Illuminate\Support\Facades\Mail;

class CalendarHelper
{

    protected static $calendar_update = [
        'name' => 'name',
        'description'=> 'description',
        'color_id' =>'color',
    ];

    /**
     * @param Request $request
     * @param $user_id
     * @return Calendar
     * @throws \Exception
     */
    public function createCalendar(Request $request, $user_id): Calendar
    {
        $calendar = new Calendar;

        foreach (self::$calendar_update as $code=>$calendat_params) {
            if ($request->has($calendat_params)) {
                if (($calendar_param = $request->get($calendat_params)) && $calendar_param !== 'undefined') {
                    $calendar->{$code} = $calendar_param;
                }
                else {
                    $empty_fields[] = $code;
                }
            }
        }

        if (isset($empty_fields))
            $this->emptyFieldsException($empty_fields);

        $calendar->owner_id = $user_id;
        $calendar->save();

        return $calendar;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return bool|Calendar
     * @throws \Exception
     */
    public function updateCalendar(int $id, Request $request)
    {
        /** @var Calendar $calendar */
        $calendar = Calendar::where('id', $id)
            ->firstOrFail();

        if ($request->user()->id != $calendar->owner_id) {
            return false;
        }

        $calendar = $this->updateModelFields($calendar, self::$calendar_update, $request);

        $curShareEmail = [];
        $newShareEmail = [];
        if ($calendar->share()) {
            $curShareEmail = $calendar->share()->pluck('email')->toArray();
        }
        if ($request->has('share')) {
            $newShareEmail = $request->get('share');
        }
        $unshare = array_diff($curShareEmail, $newShareEmail);
        $share = array_diff($newShareEmail, $curShareEmail);

        if ($share) {
            foreach ($share as $email) {
                $user = User::where('email', $email)->first();
                if ($user) {
                    $calendar->share()
                        ->attach([$user->id]);

                    $objInvited = new \stdClass();
                    $objInvited->sender = $calendar->owner->email;
                    $objInvited->receiver = $email;
                    $objInvited->event = $calendar->name;
                    $objInvited->url = env('FRONTEND_URL');
                    Mail::to($user->email)->send(new NotificationCalendar($objInvited));
                }
            }
        }
        if ($unshare) {
            foreach ($unshare as $email) {
                $user = User::where('email', $email)->first();
                $calendar->share()
                    ->detach([$user->id]);
            }
        }

        $calendar->save();

        return $calendar;
    }


    /**
     * @param integer  $id
     * @param integer  $user_id
     * @return boolean
     */
    public function deleteCalendar(int $id, int $user_id)
    {
        $calendar = Calendar::where('id', $id)
            ->firstOrFail();
        if ($calendar && $calendar->owner_id == $user_id) {
            $rez = $calendar->delete();
            return $rez;
        }
        return false;
    }

    /**
     * @param Model $model
     * @param array $update_fields
     * @param $request
     * @return Model
     * @throws \Exception
     */
    public function updateModelFields(Model $model, array $update_fields, $request)
    {
        foreach ($update_fields as $key => $code) {
            if ($request->has($code)) {
                if (($update_field = $request->get($code))) {
                    $model[$key] = $update_field;
                }
                else {
                    $empty_fields[] = $key;
                }
            }
        }

        if (isset($empty_fields))
            $this->emptyFieldsException($empty_fields);

        return $model;
    }

    /**
     * @param $empty_fields
     * @throws \Exception
     */
    public function emptyFieldsException($empty_fields) {
        $message = '';

        foreach ($empty_fields as $key => $empty_field) {
            $message .= $empty_field;
            if ($key != count($empty_fields) - 1)
                $message .= ', ';
        }

        throw new \Exception("Empty required fields: $message");
    }
}
