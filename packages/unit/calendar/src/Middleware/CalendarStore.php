<?php

namespace unit\calendar\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class CalendarStore
 *
 * @package unit\calendar\Middlewar
 */
class CalendarStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->tokenIsValid($request)) {
            return response()->json([
                'message' => 'Permission denied',
            ], Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function tokenIsValid(Request $request)
    {
        $calendar_token = config('calendar.store_token');
        $request_token = $request->header('X-CALENDAR-TOKEN');

        return !empty($calendar_token)
            && !empty($request_token)
            && $calendar_token === $request_token;
    }
}
