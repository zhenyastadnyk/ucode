<?php

use unit\calendar\Controllers\EventController;
use unit\calendar\Controllers\CalendarController;

Route::prefix('api/calendar')->group(function (){
    Route::middleware(['cors', 'auth:api'])->group(function(){
        Route::resource('add', CalendarController::class)
            ->middleware('calendar_store_middleware')
            ->only('store');
        Route::delete('/del/{id}', 'unit\calendar\Controllers\CalendarController@delete');
        Route::get('/{id}', 'unit\calendar\Controllers\CalendarController@show');
        Route::put('/{id}', 'unit\calendar\Controllers\CalendarController@update');
        Route::get('/all/get', 'unit\calendar\Controllers\CalendarController@show_all');

        Route::get('/color/get', 'unit\calendar\Controllers\CalendarController@color');
    });
});

Route::prefix('api/event')->group(function (){
    Route::middleware(['cors', 'auth:api'])->group(function(){
        Route::resource('add', EventController::class)
            ->middleware('calendar_store_middleware')
            ->only('store');
        Route::delete('/del/{id}', 'unit\calendar\Controllers\EventController@delete');
        Route::get('/{id}', 'unit\calendar\Controllers\EventController@show');
        Route::put('/{id}', 'unit\calendar\Controllers\EventController@update');
        Route::get('/all/get', 'unit\calendar\Controllers\EventController@show_all');
        Route::get('/search/get', 'unit\calendar\Controllers\EventController@search');

        Route::get('/category/get', 'unit\calendar\Controllers\EventController@category');
    });
});
