<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventIdAndUserIdForeignKeyOndeletePropertyToEventInvitedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_invited', function (Blueprint $table) {
            $table->dropForeign('event_invited_event_id_foreign');
            $table->dropForeign('event_invited_user_id_foreign');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_invited', function (Blueprint $table) {
            $table->dropForeign('event_invited_event_id_foreign');
            $table->dropForeign('event_invited_user_id_foreign');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
