<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarShareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_share', function (Blueprint $table) {
            $table->increments('calendar_share_id');
            $table->integer('calendar_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('calendar_id')->references('id')->on('calendars');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unique(['calendar_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_share');
    }
}
