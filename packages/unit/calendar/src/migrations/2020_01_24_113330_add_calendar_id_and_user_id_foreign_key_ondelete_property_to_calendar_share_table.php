<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalendarIdAndUserIdForeignKeyOndeletePropertyToCalendarShareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendar_share', function (Blueprint $table) {
            $table->dropForeign('calendar_share_calendar_id_foreign');
            $table->dropForeign('calendar_share_user_id_foreign');
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendar_share', function (Blueprint $table) {
            $table->dropForeign('calendar_share_calendar_id_foreign');
            $table->dropForeign('calendar_share_user_id_foreign');
            $table->foreign('calendar_id')->references('id')->on('calendars');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
