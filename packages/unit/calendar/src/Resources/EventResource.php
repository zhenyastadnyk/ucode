<?php

namespace unit\calendar\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CalendarResource
 *
 * @package unit\calendar\Resources
 */
class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'title'           => $this->name,
            'description'     => $this->description,
            'start'           => $this->date_from,
            'end'             => $this->date_to,
            'category'        => new CategoryResource($this->category),
            'calendar'        => new CalendarResource($this->calendar),
            'notes'           => $this->notes,
            'priority'        => $this->priority,
            'invited'         => $this->invited,
        ];
    }
}
