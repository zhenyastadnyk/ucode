<?php

namespace unit\calendar\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ColorResource
 *
 * @package unit\calendar\Resources
 */
class ColorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'code'               => $this->code,
            'title'              => $this->title,
        ];
    }
}
