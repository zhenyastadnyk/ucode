<?php

namespace unit\calendar\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**4
 * Class CalendarResource
 *
 * @package unit\calendar\Resources
 */
class CalendarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'name'               => $this->name,
            'description'        => $this->description,
            'color'              => new ColorResource($this->color),
            'owner'              => $this->owner,
            'share'              => $this->share,
        ];
    }
}
