<?php

namespace unit\calendar\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'nullable|string|max:255',
            'description'  => 'nullable|string',
            'date_from'    => 'nullable|string|max:255',
            'date_to'      => 'nullable|string|max:255',
            'category'     => 'nullable|string|max:255',
            'calendar'     => 'nullable|string|max:255',
            'invited'      => 'nullable|array',
        ];
    }
}
