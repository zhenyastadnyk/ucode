<?php

namespace unit\calendar\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEventPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string|max:255',
            'description'  => 'required|string',
            'date_from'    => 'required|string|max:255',
            'date_to'      => 'required|string|max:255',
            'notes'        => 'nullable|string',
            'priority'     => 'nullable|string|max:255',
            'invited'      => 'nullable|array',
        ];
    }
}
