<?php

namespace unit\calendar\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCalendarPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'nullable|string|max:255',
            'description'  => 'nullable|string',
            'color'        => 'nullable|string|max:255',
            'share'        => 'nullable|array',
        ];
    }
}
