<?php

namespace unit\calendar\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCalendarPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|string|max:255',
            'description'  => 'required|string',
            'color'        => 'required|string|max:255',
            'owner'        => 'required|string|max:255',
        ];
    }
}
