<?php

namespace unit\calendar\Jobs;

use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use unit\calendar\Models\Calendar;
use unit\calendar\Models\Category;
use unit\calendar\Models\Event;
use Exception;

class SaveHolidayEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    private $offset;

    /**
     * @var
     */
    private $count;

    /**
     * @var
     */
    private $limit;

    /**
     * @var
     */
    private $year;

    /**
     * SaveHolidayEventJob constructor.
     *
     * @param $count
     * @param $offset
     * @param $limit
     * @param $year
     */
    public function __construct($count, $offset, $limit, $year)
    {
        $this->offset = $offset;
        $this->count = $count;
        $this->limit = $limit;
        $this->year = $year;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->offset < $this->count) {
            $holiday_event = $this->getHolidayEvent($this->year);
            if ($holiday_event)
                $this->saveHolidayEvents($holiday_event, $this->year);

            $this->offset += $this->limit;
            $this->year += $this->limit;

            self::dispatch($this->count, $this->offset, $this->limit, $this->year);
        }
    }

    /**
     * @param array $holiday_events
     * @param int   $year
     *
     * @throws Exception
     */
    private function saveHolidayEvents(array $holiday_events, int $year)
    {
        foreach ($holiday_events['response']['holidays'] as $holiday_event) {
            $date_time = $holiday_event['date']['datetime'];
            $date_from = Carbon::parse("$date_time[year]-$date_time[month]-$date_time[day]");

            try {
                Event::firstOrCreate([
                    'name'               => $holiday_event['name'],
                    'description'        => $holiday_event['description'],
                    'date_from'          => $date_from->toDateTimeString(),
                    'category_id'        => Category::whereCode('holiday')
                        ->first()->id,
                    'date_to'            => $date_from->addDay()->addSeconds(-1)
                        ->toDateTimeString(),
                    'calendar_id'        => Calendar::whereName('Holidays')
                        ->first()->id
                ]);
            } catch (Exception $exception) {
                continue ;
            }
        }
    }

    /**
     * @param int $year
     *
     * @return mixed|null
     */
    private function getHolidayEvent(int $year)
    {
        $holiday_event = @file_get_contents('https://calendarific.com/api/v2/holidays?' .
            'api_key=' . env('CALENDARIFIC_API_KEY') .
            '&country=UA' .
            '&year=' . (string)$year
        );

        return $holiday_event ? json_decode($holiday_event, true) : null;
    }
}
