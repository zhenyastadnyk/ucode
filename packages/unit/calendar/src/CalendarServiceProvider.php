<?php

namespace unit\calendar;

use Illuminate\Support\ServiceProvider;
use unit\calendar\Helpers\CalendarHelper;
use unit\calendar\Helpers\EventHelper;
use unit\calendar\Middleware\CalendarStore;
use unit\calendar\Commands\GetHolidayEventCommand;

class CalendarServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('calendar', CalendarHelper::class);
        $this->app->bind('event', EventHelper::class);
        $this->app->bind('calendar_store_middleware', CalendarStore::class);
    }

    public function boot()
    {
        $this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');
        $this->loadMigrationsFrom(realpath(dirname(__FILE__)) . '/migrations');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/calendar.php') => config_path('calendar.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/unit/calendar/') => base_path('packages/unit/calendar')
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                GetHolidayEventCommand::class,
            ]);
        }
    }
}
