<?php

namespace unit\calendar\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'code', 'status'
    ];

    public function event()
    {
        return $this->hasMany(Event::class);
    }


}

