<?php

namespace unit\calendar\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Calendar extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'color_id', "owner_id"
    ];

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function owner() {
        return $this->belongsTo(User::class, "owner_id");
    }

    public function event() {
        return $this->belongsTo(Event::class);
    }

    public function share() {
        return $this->belongsToMany(User::class, "calendar_share");
    }

}

