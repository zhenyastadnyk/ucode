<?php

namespace unit\calendar\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Event extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', "date_from", "date_to", "calendar_id", "category_id", "notes", "priority"
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, "category_id");
    }

    public function calendar() {
        return $this->belongsTo(Calendar::class, "calendar_id");
    }

    public function invited() {
        return $this->belongsToMany(User::class, 'event_invited');
    }
}

