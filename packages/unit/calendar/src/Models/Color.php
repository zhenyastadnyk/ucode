<?php

namespace unit\calendar\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'code'
    ];

    public function calendars()
    {
        return $this->hasMany(Calendar::class);
    }
}

