<?php


namespace unit\calendar\Commands;

use DateTime;
use DateTimeZone;
use Illuminate\Console\Command;
use unit\calendar\Jobs\SaveHolidayEventJob;

class GetHolidayEventCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday-event:save {years_number}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get national holidays';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $years_number = $this->argument('years_number');

        if (!is_numeric($years_number))
            dd("Error: enter number of years you want to get before and after the current year!");

        $current_year = date('Y');
        $count = $years_number * 2 + 1;
        $year = $current_year - $years_number;

        SaveHolidayEventJob::dispatch($count, 0, 1, $year);
    }
}
