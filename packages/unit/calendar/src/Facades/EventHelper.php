<?php

namespace unit\calendar\Facades;

use Illuminate\Support\Facades\Facade;

class EventHelper extends Facade {

    public static function getFacadeAccessor()
    {
        return 'event';
    }
}
