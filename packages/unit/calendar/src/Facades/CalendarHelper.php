<?php

namespace unit\calendar\Facades;

use Illuminate\Support\Facades\Facade;

class CalendarHelper extends Facade {

    public static function getFacadeAccessor()
    {
        return 'calendar';
    }
}
