<?php
Route::prefix('api/auth')->group(function (){
    Route::middleware(['cors'])->group(function(){
        Route::post('register', 'unit\auth\Controllers\AuthController@register');
        Route::get('confirm/{id?}', 'unit\auth\Controllers\AuthController@verify')
            ->name('verification.verify')
            ->middleware('signed');
        Route::get('resend', 'unit\auth\Controllers\AuthController@resendVerify');
        Route::post('login', 'unit\auth\Controllers\AuthController@login');
        Route::post('logout', 'unit\auth\Controllers\AuthController@logout')->middleware('auth:api');

        Route::get('userdata', 'unit\auth\Controllers\AuthController@user')->middleware('auth:api', 'verified');
        Route::post('resetpassword', 'unit\auth\Controllers\ForgotPasswordController@sendResetLinkEmail');
        Route::post('changepassword', 'unit\auth\Controllers\ResetPasswordController@reset');
    });
});
