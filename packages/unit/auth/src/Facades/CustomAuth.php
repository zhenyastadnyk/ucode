<?php

namespace unit\auth\Facades;

use Illuminate\Support\Facades\Facade;

class CustomAuth extends Facade {

    public static function getFacadeAccessor()
    {
        return 'custom_auth';
    }
}
