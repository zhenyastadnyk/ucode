<?php

namespace unit\auth\Controllers;

use App\Http\Controllers\Controller;
use unit\auth\Requests\LoginPostRequest;
use unit\auth\Requests\RegisterPostRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use unit\auth\Facades\CustomAuth;
use unit\calendar\Models\Calendar;
use unit\auth\Resources\AuthResource;

/**
 * Class AuthController
 *
 * @package unit\auth\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterPostRequest $request)
    {
       $data = $request->all();
        try {
            $user =  CustomAuth::createNewUser($data);

            return response()
                ->json(new AuthResource($user));
        } catch (\Exception $exception) {
            return response()->json([
                'message' => ["general"=>$exception->getMessage()],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify()
    {
        $user = User::find(request('id'));
        if ($user && CustomAuth::verify($user)) {
            Calendar::updateOrCreate([
                'name'               => 'default',
                'description'        => 'default',
                'color_id'           => 1,
                'owner_id'           => $user->id
            ]);

            $admin_user = User::whereEmail('admin@admin')->first();
            $calendar = Calendar::whereName('Holidays')->first();
            if ($admin_user && !$calendar->share()->find($user->id)) {
                $calendar->share()
                    ->attach([$user->id]);
            }
        }
        return $user && CustomAuth::verify($user)
            ? response()->json(null, Response::HTTP_OK)
            : response()->json(null, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerify(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();

        if (!$user) {
            return response()->json([
                'message' => ['general' => trans('auth.error.not_find')],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return CustomAuth::sendVerifyMail($user)
            ? response()->json(null, Response::HTTP_OK)
            : response()->json([
                'message' => ['general' => trans('auth.error.verified')],
            ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginPostRequest $request)
    {
        if (!Auth::attempt(request(['email', 'password']))) {
            return response()->json(
                ['message' =>["general"=>trans('auth.error.wrong_credentials')]],
                Response::HTTP_UNAUTHORIZED);
        }
        if (!CustomAuth::isVerified($request->user())) {
            return response()
                ->json(['message' => ["general"=>trans('auth.error.user_not_verified')]], Response::HTTP_FORBIDDEN);
        }

        return response()->json(CustomAuth::getToken($request->user()));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(null, Response::HTTP_OK);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

}
