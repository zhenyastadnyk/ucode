Hello <i>{{ $notification->receiver }}</i>,
<p>You have been invited to an event</p>

<p>
    You have been invited to an event {{ $notification->event}}
    on {{ $notification->url}}
</p>

Thank You,
<br/>
<i>{{ $notification->sender }}</i>