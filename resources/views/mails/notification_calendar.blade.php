Hello <i>{{ $notification->receiver }}</i>,
<p>You have new calendar</p>
<p>
    A new calendar is available for you {{ $notification->event}}
    on {{ $notification->url}}
</p>

Thank You,
<br/>
<i>{{ $notification->sender }}</i>