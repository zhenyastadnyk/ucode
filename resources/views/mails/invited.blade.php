Hello <i>{{ $invited->receiver }}</i>,
<p>You have been invited to an event</p>

<p>
    You can register,
    <a href="{{ $invited->url}}/register/?event={{ $invited->event}}">
        {{ $invited->url}}/register/?event={{ $invited->event}}
    </a>
    on {{ $invited->url}}
</p>

Thank You,
<br/>
<i>{{ $invited->sender }}</i>