<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}" />
    
    @include('site.layouts.styles')

</head>
<body>
    
    <div id="wrapper">
        @yield('content')
    </div>

    @include('site.layouts.scripts')
</body>
</html>