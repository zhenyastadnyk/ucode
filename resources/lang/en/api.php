<?php

return [
    'error'       => [
        'not_found' => 'Not found',
    ],
    'calendar'     => [
        'delete'            => 'Calendar delete',
        'not_permission'    => 'no permission',
        'not_params'        => 'no params',
    ],
    'event'     => [
        'delete'      => 'Event delete',
    ]
];
