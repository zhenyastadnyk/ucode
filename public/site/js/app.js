//Функция, возвращающая абсолютный url
var getAbsoluteUrl = (function () {
    var a;
    return function (url) {
        if (!a) a = document.createElement('a');
        a.href = url;
        return a.href;
    };
})();

//AJAX отправка email через форму обратной связи
$(document).ready(function(){
    $('#contactform').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/feedback',
            data: $('#contactform').serialize(),
            success: function(data){
                if(data.result)
                {
                    $('#senderror').hide();
                    $('#sendmessage').show();
                    $('#name').html(data.name);
                }
                else
                {
                    $('#senderror').show();
                    $('#sendmessage').hide();
                }
            },
            error: function(err){
                $('#senderror').show();
                $('#sendmessage').hide();
            }
        });
    });
    $("#phone").mask("+38(999) 999-9999");
});
