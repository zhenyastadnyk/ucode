<?php

use Illuminate\Database\Seeder;
use unit\calendar\Models\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create([
            'title' => 'Radicchio',
            'code' => '#AD1457'
        ]);
        Color::create([
            'title' => 'Tangerine',
            'code' => '#F4511E'
        ]);
        Color::create([
            'title' => 'Citron',
            'code' => '#E4C441'
        ]);
        Color::create([
            'title' => 'Basil',
            'code' => '#0B8043'
        ]);
        Color::create([
            'title' => 'Blueberry',
            'code' => '#3F51B5'
        ]);
        Color::create([
            'title' => 'Cherry Blossom',
            'code' => '#D81B60'
        ]);
        Color::create([
            'title' => 'Pumpkin',
            'code' => '#EF6C00'
        ]);
        Color::create([
            'title' => 'Avocado',
            'code' => '#C0CA33'
        ]);
        Color::create([
            'title' => 'Eucalyptus',
            'code' => '#009688'
        ]);
        Color::create([
            'title' => 'Lavender',
            'code' => '#7986CB'
        ]);
        Color::create([
            'title' => 'Cocoa',
            'code' => '#795548'
        ]);
    }
}
