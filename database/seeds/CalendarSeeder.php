<?php

use App\User;
use Illuminate\Database\Seeder;
use unit\calendar\Models\Calendar;

class CalendarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Calendar::create([
            'name'               => 'Holidays',
            'description'        => 'holidays',
            'color_id'           => 2,
            'owner_id'           => User::whereEmail('admin@admin')->first()->id
        ]);
    }
}
