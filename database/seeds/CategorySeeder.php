<?php

use Illuminate\Database\Seeder;
use unit\calendar\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'title' => 'Arrangement',
            'code' => 'arrangement',
            'status' => 1
        ]);
        Category::create([
            'title' => 'Reminder',
            'code' => 'reminder',
            'status' => 1
        ]);
        Category::create([
            'title' => 'Task',
            'code' => 'task',
            'status' => 1
        ]);
        Category::create([
            'title' => 'Holiday',
            'code' => 'holiday',
            'status' => 0
        ]);
    }
}
